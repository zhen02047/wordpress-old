<?php
/*
Template Name:old-service
*/
?>
<?php get_header(); ?> 
      <div class="container">
  <h1>各局處計畫</h1>
  <hr class="hr1">
  
<div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>編號</th>
        <th>社區名稱</th>
        <th>特色</th>
        <th>提供服務</th>
      </tr>
    </thead>
    <tbody>

<?php
$args = array(
 'post_type' => 'service',
 'posts_per_page' => 20 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
      <tr>
        <td><p><?php the_field('no'); ?></p></td>
        <td><p><?php the_field('name'); ?></p></td>
        <td><p><?php the_field('feature'); ?></p></td>
        <td><p><?php the_field('service'); ?></p></td>
      </tr>
<?php endwhile;?>
<?php wp_reset_query();?>
     
    </tbody>
  </table>
  </div>
</div>
</div>
<?php get_footer(); ?> 
  <script src="<?php
                bloginfo('template_directory') ?>/js/jquery.min.js"></script>
    <script src="<?php
                bloginfo('template_directory') ?>/js/bootstrap.bundle.min.js"></script>
</body>
</html>