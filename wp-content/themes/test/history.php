<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<?php
/*
Template Name:old-history
*/
?>
<?php get_header(); ?> 
<div class="container mb-50">
  <h1>歷史緣起</h1>
  <hr class="hr1">
    <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#first">成立緣起</a></li>
    <li><a data-toggle="tab" href="#menu1">推對家園站動機</a></li>
    <li><a data-toggle="tab" href="#menu2">執行內容及成效</a></li>
  </ul>

  <div class="tab-content">
    <div id="first" class="tab-pane fade in active">
      <p class="m-t-30">為因應高齡化趨勢，建構高齡者照護政策，本縣參照世界衛生組織揭示之高齡友善城市八大面向「無障礙與安全的公共空間」、「交通運輸」、「住宅」、「社會參與」、「敬老與社會融入」、「工作與志願服務」、「通訊與資訊」、「社區及健康服務」等，逐步推動高齡友善城市。於100年簽署世界衛生組織高齡友善城市全球網絡及愛爾蘭成功老化網絡之「都柏林宣言」，倡議對高齡議題之重視與支持</p>
      <p>101年為多元性廣納長者意見結合保健志工進行65歲以上長者問卷調查(計完成6,052份)。從調查中得知，宜蘭長者對八大面向的滿意度以「住宅」最高、其次分別為「敬老與社會融入」、「通訊和資訊」，友善程度得分以「工作與志願服務」與「社會參與」兩項較低。</p>
      <p>於需要改善程度方面，以「交通運輸」與「無障礙與安全公共空間」較高。基此，102-103年依長者需求建立34項指標，並發展35項行動計畫。並依社區化需求整合縣府各局處、專家學者以及NGO夥伴能量，推動社區化高齡友善城市~老來寶健康促進家園，將社區營造成一個符合「老人友善城市」八大面向的健康社區，促進在地老化，透過跨領域資源連結的合作模式，建置一個健康促進平台，結合來自教育單位、警消單位、衛生單位、社政單位、勞工單位、民間團體、社區及志工的代表等推動高齡友善服務，協力參與活躍老化健康促進的工作當中，產生一加一大於二的效果，為本縣長者創造「更優質、更活躍、更宜居」的高齡友善環境。</p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <p class="m-t-30">宜蘭縣在79年即進入高齡化社會，比台灣地區提早3年之久。依據內政部人口資料，106年12月底全國65歲以上高齡人數3,268,013人，占總人口比率13.86%;而宜蘭縣2017年12月底65歲以上高齡人數為70,025人，占總人口比率15.31%。比全國還高出1.45%，顯示高齡化問題日趨嚴重。</p>
      <p>而老化是一種退化性、慢性及新陳代謝性的演變過程，也是每一個人必須經歷的過程，老人身體機能老化，若未能加以維護很容易就會面臨急速退化及各種慢性疾病的威脅，導致生活問題的增加與心理調適困難等問題。在西方，除了原有的醫療保健與長期照護外，還包括健康促進（health promotion）。由此可知，未來老人照顧服務比目前將更為多元，從預防保健、急性醫療、長期照護、到健康老人的休閒養生，均為照顧的範圍，如何加以整合，有效提供老人連續性、無縫接軌的照顧，是值得正視的議題。</p>
      <p>
        本縣透過「高齡友善城市」社區化整合型服務，以長者居住之社區據點為推動場域，進行跨局處之連結與整合，如結合工旅處進行社區周邊增加行人道路鋪設及綠美化設計，消防局針對長者補助住宅用火災警報器提供長者緊急救援系統服務，社會處長青食堂提供長者營養午餐，衛生局成立長青友善關懷站服務團隊、不老診所、失智照顧關懷據點、居家防跌訪視、提供居家防滑墊及小夜燈、扶手裝置、銀髮平衡班，長照所提供失智症預防課程及各項長照服務，計畫處資訊電腦班，警察局社區路口交通號誌等，透過跨部門(跨領域)資源整合合作以提供長者友善環境為目標，營造高齡友善城市。
      </p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <p class="m-t-30">老來寶健康促進家園站是以65歲以上社區長者為關懷及照護對象，運用健康促進營造的手法，以衛生所為平台，透過縣政府跨部門的參與，相關領域專家等協同合作，依循評估、診斷的步驟，規劃老人健康願景，研訂老來寶健康促進家園執行方案，達以下成效：</p>
      <p>1. 推動老人不同體適能運動模式：運動對於多數慢性疾病的發生是可以有效避免的，透過有效地改善生活方式，讓長者瞭解運動的好處及對健康的益處，提昇其運動的動機，在長期規律運動後可帶來許多效益如：維持理想體重、預防心血管疾病、降低血脂、有效控制血壓、降低糖尿病風險等。</p>
      <p>2. 推動銀髮平衡班：提供平衡運動訓練，增加長輩健康體適能、平衡協調能力、增加核心肌肉群、肌耐力，提升長者防跌的認知，預防因跌倒引發的傷病。</p>
      <p>3. 辦理營養講座、口腔保健等相關健康講習課程，強調操作性、從自身做起的生活行為實踐，以社區人際網絡為基礎，團體學習形成的支持系統。</p>
      <p>4. 結合不老診所醫師，針對長者的罹病及用藥進行評量，及早發現長者生活及健康風險。</p>
      <p>5. 透過長者居家安全評估及生理功能評估調查、以長者需求為基礎，於浴室廁所裝置扶手及小夜燈，提供長者安全無障礙之居住環境。</p>
      <p>6. 結合12鄉鎮市衛生所公共衛生護士與社區關懷訪視員及長照服務員，形成人力資源網絡，發展「特殊照顧需求」為中心的社區關懷照顧服務網絡，並提供服務資源使用與轉介，讓縣內脊髓損傷之身心障礙者、獨居長者、失智症長者及弱勢族群等能在社區中活得獨立自主又有尊嚴，獲得在地且妥適的照顧服務。</p>
      <p>7. 鼓勵長輩走出戶外社會參與，透過人際關係的互動增進自我信心及價值。</p>
    </div>
  </div>
</div> 

<?php get_footer(); ?> 
</body>
</html>