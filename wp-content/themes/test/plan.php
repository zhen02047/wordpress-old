<?php
/*
Template Name:old-plan
*/
?>
<?php get_header(); ?> 
<div class="container">
  <h1>各局處計畫</h1>
  <hr class="hr1">
  
<div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>發布時間</th>
        <th>計畫名稱</th>
        <th>載點</th>
        <th>公告單位</th>
      </tr>
    </thead>
    <tbody>

<?php
$args = array(
 'post_type' => 'plan',
 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
      <tr>
        <td><p><?php the_field('time'); ?></p></td>
        <td><p><?php the_field('name'); ?></p></td>
        <td><p><?php the_field('link'); ?></p></td>
        <td><p><?php the_field('unit'); ?></p></td>
      </tr>
<?php endwhile;?>
<?php wp_reset_query();?>
     
    </tbody>
  </table>
  </div>
</div>
</div>

<?php get_footer(); ?> 