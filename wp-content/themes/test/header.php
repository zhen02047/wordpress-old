<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel='stylesheet prefetch' href='<?php
              bloginfo('template_directory') ?>/css/bootstrap.min.css'>
  <link href="<?php
              bloginfo('template_directory') ?>/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php
              bloginfo('template_directory') ?>/css/modern-business.css">
    <link rel="stylesheet" type="text/css" href="<?php
              bloginfo('template_directory') ?>/css/style.css">
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-yellow">
        <div class="container">
          <a class="navbar-brand" href="/"><img style="width: 120px" src="<?php
              bloginfo('template_directory') ?>/img/nav_logo4.png"></a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                關於家園站
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href="history/">歷史緣起</a>
                <a class="dropdown-item" href="portfolio-2-col.html">特色服務</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="unit/">老來寶家園站</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="teacher/">師資介紹</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="plan/">各局處計畫</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>