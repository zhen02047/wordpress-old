<?php
/*
Template Name:old-unit
*/
?>
<?php get_header(); ?> 
<div class="container">
  <h1>老來寶家園站</h1>
  <hr class="hr1">
</div>
<div class="container">
  <div class="row">
    <?php
	$args = array(
 		'post_type' => 'unitpoint',
 		'posts_per_page' => 9 );
	$loop = new WP_Query( $args );
	?>
	<?php
		while ( $loop->have_posts() ) : $loop->the_post();?>
		<div class="col-md-4">
			<div class="unit">
  				<img class="img-fluid d-block mx-auto " src="<?php the_field('img');?>" alt="" width=350 >
  				<p class="p-l-10 p-t-20"><?php the_field('name');?></p>
  				<p class="p-l-10"><span>地點:</span><?php the_field('address');?></p>
  				<p class="p-l-10"><span>時間:</span><?php the_field('time');?></p>
  				<p class="p-l-10 p-b-20"><span>電話:</span><?php the_field('tel');?></p>
  			</div>
		</div>
	<?php endwhile;?>
  </div>
</div>
<?php get_footer(); ?> 