    <footer>
      <div class="container-fluid p-0">
              <img class="img-fluid" style="width: 100%" src="<?php
                        bloginfo('template_directory') ?>/img/footer-1.png">
      </div>
      <div class="container-fluid">
              <div class="row footer-bg">
              <div class="col-md-6">
      			<p>
					聯絡地址:(26060)宜蘭縣宜蘭市縣政北路一號<br>連絡電話:(總機):(03)925-1000
				</p>
      		 </div>
      		 <div class="col-md-6 mr-0">
      			<p>			
					為民服務電話:縣境直播1999(付費專線、以市話費率計收)<br>服務時間:週一至週五8:00-17:30
      			</p>
      		</div>
      		</div>
      </div>
    </footer>
      <script src="<?php
                bloginfo('template_directory') ?>/js/jquery.min.js"></script>
    <script src="<?php
                bloginfo('template_directory') ?>/js/bootstrap.bundle.min.js"></script>
  <script src="<?php
                bloginfo('template_directory') ?>/js/bootstrap.js"></script>