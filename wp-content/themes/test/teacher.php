<?php
/*
Template Name:old-teacher
*/
?>
<?php get_header(); ?> 
<div class="container">
  <h1>師資介紹</h1>
  <hr class="hr1">
</div>
<section class="">
<div class="container">
  <div class="row">
    <div class="col-lg-12">
          <h2 class="my-4">平衡班</h2>
    </div>
<?php
$args = array(
 'post_type' => 'balance',
 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
<div class="col-lg-4 col-sm-6 text-center mb-4">
  <img class="rounded-circle img-fluid d-block mx-auto" src="<?php the_field('img');?>" alt="" width="200">
  <h3><?php the_field('text');?></h3>
  <p><?php the_field('name');?></p>
</div>
<?php endwhile;?>
  </div>
</div>
</section>
<section class="bg-light">
<div class="container">
  <div class="row">
    <div class="col-lg-12">
          <h2 class="my-4">桌遊</h2>
    </div>
<?php
$args = array(
 'post_type' => 'game',
 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
<div class="col-lg-4 col-sm-6 text-center mb-4">
  <img class="rounded-circle img-fluid d-block mx-auto" src="<?php the_field('img');?>" alt="" width="200">
  <h3><?php the_field('text');?></h3>
  <p><?php the_field('name');?></p>
</div>
<?php endwhile;?>
  </div>
</div>
</section>
<section class="">
<div class="container">
  <div class="row">
    <div class="col-lg-12">
          <h2 class="my-4">智慧長者</h2>
    </div>
<?php
$args = array(
 'post_type' => 'older',
 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
<div class="col-lg-4 col-sm-6 text-center mb-4">
  <img class="rounded-circle img-fluid d-block mx-auto" src="<?php the_field('img');?>" alt="" width="200">
  <h3><?php the_field('text');?></h3>
  <p><?php the_field('name');?></p>
</div>
<?php endwhile;?>
  </div>
</div>
</section>
<?php get_footer(); ?> 
  <script src="<?php
                bloginfo('template_directory') ?>/js/jquery.min.js"></script>
    <script src="<?php
                bloginfo('template_directory') ?>/js/bootstrap.bundle.min.js"></script>
</body>
</html>