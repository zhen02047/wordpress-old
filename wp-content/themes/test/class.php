<?php
/*
Template Name:digi-class
*/
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Digi</title>

    <!-- Bootstrap core CSS -->
   <!--  <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php
bloginfo('template_directory') ?>/css/bootstrap.min.css"
media="screen" rel="stylesheet" type="text/css" />
    <!-- Custom styles for this template -->

    <link href="<?php
bloginfo('template_directory') ?>/css/agency.min.css"
media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php
bloginfo('template_directory') ?>/css/modern-business.css"
media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php
bloginfo('template_directory') ?>/css/style.css"
media="screen" rel="stylesheet" type="text/css" />
  </head>
  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="<?php
              bloginfo('template_directory') ?>/img/nav_logo.png"'"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">課程總覽</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">直播總表</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">全球Moocs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">合作夥伴</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">課程地圖</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger m-t-5" href="#">我的課程</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger login" href="#"><button  class="login-btn">登入</button></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="header">
        <div class="container">
          <p>全球Moocs學習平台 </p>
        </div>
     </div>
    <section>
      <div class="row">
        <div class="container">
          <div class="col-md-12 text-center">
              <img src="<?php
                bloginfo('template_directory') ?>/img/class_banner.png"'">
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <table class="table table-striped">
          <tbody>
            <tr>
            <?php
            $args = array(
                'post_type' => 'class_name',
                'posts_per_page' => 10 );
                $loop = new WP_Query( $args );
                ?>
                 <?php
              while ( $loop->have_posts() ) : $loop->the_post();?>
              <td><p><?php the_field('class_name');?></p>
                   <p><?php the_field('class_name_en');?></p>
              </td>
              <td>
                    <p><?php the_field('class_des');?></p>
              </td>
              </tr>
            <?php endwhile;?>

            
          </tbody>
        </table>
      </div>
    </section>

           
    <!-- Footer -->
    <footer class="py-5 footer-bg">
      <div class="container">
        <hr class="footer-line">
        <div class="footer-des col-md-9 col-sm-12">
            <p class="m-0 text-left text-white" style="font-size: 14px;">
              Copyright ©2017-2020 跨域數位人才加速躍升計畫辦公室 網頁系統營運及維護：<br>如有任何問題或建議，請洽網站維護人員
            </p>
        </div>
        <div class="social text-right col-md-3 col-sm-12">
          <a href=""><img src="<?php
              bloginfo('template_directory') ?>/img/footer_icon_fb.png"'"></a>
          <a href=""><img src="<?php
              bloginfo('template_directory') ?>/img/footer_icon_yt.png"'"></a>
          <a href=""><img src="<?php
              bloginfo('template_directory') ?>/img/footer_icon_home.png"'"></a>

    
        </div>
        <div class="clear"></div>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php
      bloginfo('template_directory') ?>/js/jquery.min.js"></script>
    <script src="<?php
      bloginfo('template_directory') ?>/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="<?php
      bloginfo('template_directory') ?>/js/jquery.easing.min.js"></script>
    <!-- Contact form JavaScript -->
    <script src="<?php
      bloginfo('template_directory') ?>/js/jqBootstrapValidation.js"></script>
    <script src="<?php
        bloginfo('template_directory') ?>/js/contact_me.js"></script>
    <!-- Custom scripts for this template -->
    <script src="<?php
        bloginfo('template_directory') ?>/js/agency.min.js"></script>
  </body>

</html>


