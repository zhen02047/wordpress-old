<?php
/*
Template Name:old-index
*/
?>
<?php get_header(); ?> 
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item carousel-item-index active" style="background-image: url('<?php
              bloginfo('template_directory') ?>/img/Banner_1.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item carousel-item-index" style="background-image:url('<?php
              bloginfo('template_directory') ?>/img/Banner_2.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item carousel-item-index" style="background-image: url('<?php
              bloginfo('template_directory') ?>/img/Banner_3.png')">
            <div class="carousel-caption d-none d-md-block">

            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>
    <section>
        <div class="col-md-12 text-center">
          <p class="big-title-index">老來寶，宜蘭到處跑</p>
        </div>
          <div class="container-fluid">
             <div class="banner">
              <img class="img-fluid" src="<?php
                        bloginfo('template_directory') ?>/img/index_img-1.png">
              </div>
          </div>
    </section>
    <section>
      <div class="container">
        <div class="col-md-12 text-center">
          <p class="title-index">其他相關網站</p>
        </div>
          <div class="row">
<?php
$args = array(
 'post_type' => 'website',
 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<?php
while ( $loop->have_posts() ) : $loop->the_post();?>
<div class="col-lg-3 col-sm-6 text-center mb-4">
  <div class="border-yellow">
  <img class="rounded-circle img-fluid d-block mx-auto " src="<?php the_field('website-logo');?>" alt="" width="150">
  </div>
  <p><?php the_field('website-name');?></p>
</div>
<?php endwhile;?>
  </div>
  </div>
    </section>
<?php get_footer(); ?> 
  <script src="<?php
                bloginfo('template_directory') ?>/js/jquery.min.js"></script>
    <script src="<?php
                bloginfo('template_directory') ?>/js/bootstrap.bundle.min.js"></script>
</body>
</html>